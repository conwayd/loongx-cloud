## 项目说明

#### 项目目标
> 我们心中最好的 spring cloud 开源架构

#### 项目宗旨
> 用最正确的思想，实现最好的效果，拒绝繁琐，拒绝冗余，尽可能发挥资源的价值。用心设计每一行代码，我们出品，必属精品。

#### 项目特性
* 传统jar方式部署，支持docker、docker-compose
* graalvm原生方式暂不支持（当前局限性很大，很多生态组件有问题，hint配置复杂）
* 业务服务采用响应式框架webflux

#### 适用场景
> 中大型互联网项目后端服务

#### 架构图
![整体架构图](https://gitee.com/conwayd/network-files/raw/master/pictures/%E6%9E%B6%E6%9E%84%E8%AE%BE%E8%AE%A1.jpg "整体架构图")

#### 项目结构

~~~
loongx-cloud
├── apis                                      // 公共接口
├── commons                                   // 公共依赖
│     └── common-cache                        // 公共缓存模块
│     └── common-core                         // 公共基础模块
│     └── common-datasource                   // 公共数据源模块
│     └── common-mq                           // 公共消息队列模块
│     └── common-operationlog                 // 公共操作日志模块
│     └── common-rpc                          // 公共微服务调用模块
│     └── common-security                     // 公共安全模块
│     └── common-web                          // 公共业务服务模块
│     └── common-wx                           // 公共微信模块
│     └── common-zfb                          // 公共支付宝模块
├── services                                  // 服务模块
│     └── config-server                       // 配置中心服务
│     └── register-server                     // 注册中心服务
│     └── base-server                         // 基础业务服务
~~~

#### 环境
> 基础：[jdk17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html) + [spring boot 3.2.3](https://docs.spring.io/spring-boot/docs/3.2.3/reference/html/) + [spring cloud 2023.0.0](https://docs.spring.io/spring-cloud-release/reference/2023.0.0/index.html)

> 业务服务架构：[spring webflux](https://docs.spring.io/spring-framework/reference/web/webflux.html) + [spring r2dbc](https://spring.io/projects/spring-data-r2dbc)

> 服务注册与发现：[spring eureka](https://spring.io/projects/spring-cloud-netflix)

> 配置中心: [Spring Cloud Config](https://spring.io/projects/spring-cloud-config)

> 网关：[spring cloud gateway](https://spring.io/projects/spring-cloud-gateway)

> 接口文档: [smart-doc](https://gitee.com/TongchengOpenSource/smart-doc)

