/*
 * loongx-cloud
 *
 * Copyright 2024 conwayd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.conwayd.baseserver;

import com.conwayd.baseserver.entity.SysUser;
import com.conwayd.common.core.utils.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;

/**
 * TestJson
 *
 * @author conwayD at 2024-03-05 09:25
 * @since 1.0.0
 */
public class JsonTest {

    public static void main(String[] args) {
        String s = "[{\"id\":1,\"uid\":\"bba95c2db1e711ee93e2fa163e146c40\",\"userName\":\"admin\",\"nickName\":\"admin\",\"orgId\":0,\"orgUid\":\"\",\"orgTreeCode\":\"\",\"phone\":\"12345678901\",\"sex\":2,\"birth\":null,\"email\":\"\",\"avatar\":\"\",\"password\":\"$2a$10$6jJc/HmDqIpRhT0ICBMhYOYKF3I6ImIZmtKZh34wJzIGZYqvI4Cxa\",\"loginIp\":null,\"loginTime\":null,\"note\":\"\",\"status\":0,\"delFlag\":0,\"created\":\"2023-11-01 17:07:11\",\"createdMilli\":1698829631953,\"createdBy\":0,\"updated\":\"2023-12-23 16:41:29\",\"updatedMilli\":1703320888729,\"updatedBy\":0,\"userId\":1,\"roleId\":1},{\"id\":3,\"uid\":\"bba95c2db1e711ee93e2fa163e146c41\",\"userName\":\"a\",\"nickName\":\"aaa\",\"orgId\":0,\"orgUid\":\"\",\"orgTreeCode\":\"\",\"phone\":\"12345678901\",\"sex\":2,\"birth\":null,\"email\":\"\",\"avatar\":\"\",\"password\":\"$2a$10$6jJc/HmDqIpRhT0ICBMhYOYKF3I6ImIZmtKZh34wJzIGZYqvI4Cxa\",\"loginIp\":null,\"loginTime\":null,\"note\":\"\",\"status\":0,\"delFlag\":0,\"created\":\"2023-11-01 17:07:11\",\"createdMilli\":1698829631953,\"createdBy\":0,\"updated\":\"2023-12-23 16:41:29\",\"updatedMilli\":1703320888729,\"updatedBy\":0,\"userId\":null,\"roleId\":null}]";
        String ss = "[{\"id\":\"1\",\"uid\":\"bba95c2db1e711ee93e2fa163e146c40\",\"userName\":\"admin\",\"nickName\":\"admin\",\"orgId\":\"0\",\"orgUid\":\"\",\"orgTreeCode\":\"\",\"phone\":\"12345678901\",\"sex\":2,\"email\":\"\",\"avatar\":\"\",\"password\":\"$2a$10$6jJc/HmDqIpRhT0ICBMhYOYKF3I6ImIZmtKZh34wJzIGZYqvI4Cxa\",\"note\":\"\",\"status\":0,\"delFlag\":0,\"created\":\"2023-11-01 17:07:11\",\"createdMilli\":\"1698829631953\",\"createdBy\":\"0\",\"updated\":\"2023-12-23 16:41:29\",\"updatedMilli\":\"1703320888729\",\"updatedBy\":\"0\",\"userId\":\"1\",\"roleId\":\"1\"},{\"id\":\"3\",\"uid\":\"bba95c2db1e711ee93e2fa163e146c41\",\"userName\":\"a\",\"nickName\":\"aaa\",\"orgId\":\"0\",\"orgUid\":\"\",\"orgTreeCode\":\"\",\"phone\":\"12345678901\",\"sex\":2,\"email\":\"\",\"avatar\":\"\",\"password\":\"$2a$10$6jJc/HmDqIpRhT0ICBMhYOYKF3I6ImIZmtKZh34wJzIGZYqvI4Cxa\",\"note\":\"\",\"status\":0,\"delFlag\":0,\"created\":\"2023-11-01 17:07:11\",\"createdMilli\":\"1698829631953\",\"createdBy\":\"0\",\"updated\":\"2023-12-23 16:41:29\",\"updatedMilli\":\"1703320888729\",\"updatedBy\":\"0\"}]";

        TypeReference<List<SysUser>> typeReference = new TypeReference<>() {
        };

//        List<SysUser> list = JsonUtil.fromJson(s, typeReference, false);
        List<SysUser> list1 = JsonUtil.fromJson(s, List.class, false);

        System.out.println(JsonUtil.toJson(list1));

        List<SysUser> list2 = JsonUtil.fromJson(ss, typeReference, false);

        System.out.println(JsonUtil.toJson(list2));
    }

}
