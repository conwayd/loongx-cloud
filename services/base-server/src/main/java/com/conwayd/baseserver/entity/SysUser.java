package com.conwayd.baseserver.entity;

import com.conwayd.common.core.utils.JsonUtil;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Table(name = "sys_user")
public class SysUser implements Serializable {

    @Serial
    private static final long serialVersionUID = -2522165712915403093L;

    /**
     * 用户id
     */
    @Id
    private Long id;
    /**
     * 数据uuid，不带 '-'
     */
    private String uid;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 所属组织id
     */
    private Long orgId;
    /**
     * 所属组织uid
     */
    private String orgUid;
    /**
     * 所属组织treeCode
     */
    private String orgTreeCode;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 生日
     */
    private LocalDate birth;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 密码
     * <br>
     * 数据库加密存储，不可解密
     */
    private String password;
    /**
     * 最后登录IP
     */
    private String loginIp;
    /**
     * 最后登录时间
     */
    private LocalDateTime loginTime;
    /**
     * 备注
     */
    private String note;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 删除标志
     * <br>
     * 使用mybatis-plus的删除api时，自动设置该属性值，无需设置
     */
    private Integer delFlag;
    /**
     * 创建时间
     * <br>
     * 自动填充，无需设置
     */
    private LocalDateTime created;
    /**
     * 创建时间毫秒数
     * <br>
     * 自动填充，无需设置
     */
    private Long createdMilli;
    /**
     * 创建人用户id
     * <br>
     * 自动填充，无需设置
     */
    private Long createdBy;
    /**
     * 更新时间
     * <br>
     * 自动填充，无需设置
     */
    private LocalDateTime updated;
    /**
     * 更新时间毫秒数
     * <br>
     * 自动填充，无需设置
     */
    private Long updatedMilli;
    /**
     * 更新人用户id
     * <br>
     * 自动填充，无需设置
     */
    private Long updatedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgUid() {
        return orgUid;
    }

    public void setOrgUid(String orgUid) {
        this.orgUid = orgUid;
    }

    public String getOrgTreeCode() {
        return orgTreeCode;
    }

    public void setOrgTreeCode(String orgTreeCode) {
        this.orgTreeCode = orgTreeCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public LocalDateTime getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(LocalDateTime loginTime) {
        this.loginTime = loginTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Long getCreatedMilli() {
        return createdMilli;
    }

    public void setCreatedMilli(Long createdMilli) {
        this.createdMilli = createdMilli;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public Long getUpdatedMilli() {
        return updatedMilli;
    }

    public void setUpdatedMilli(Long updatedMilli) {
        this.updatedMilli = updatedMilli;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return JsonUtil.toJson(this);
    }

}
