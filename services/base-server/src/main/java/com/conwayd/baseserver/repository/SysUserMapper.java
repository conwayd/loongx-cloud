package com.conwayd.baseserver.repository;

import com.conwayd.baseserver.entity.SysUser;
import com.conwayd.baseserver.entity.SysUserVO;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SysUserMapper extends R2dbcRepository<SysUser, Long> {

    Mono<SysUser> findByUserName(String name);

    @Query(value = "SELECT su.*,sur.* FROM sys_user su LEFT JOIN myxb_dev.sys_user_role sur ON su.id = sur.user_id WHERE su.id = :id")
    Flux<SysUserVO> selectWithRoleById(@NonNull @Param("id") Long id);
}
