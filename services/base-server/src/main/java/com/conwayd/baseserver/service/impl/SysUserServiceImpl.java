package com.conwayd.baseserver.service.impl;

import com.conwayd.baseserver.entity.SysUser;
import com.conwayd.baseserver.repository.SysUserMapper;
import com.conwayd.baseserver.service.SysUserService;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class SysUserServiceImpl implements SysUserService {

    private final SysUserMapper sysUserMapper;

    public SysUserServiceImpl(SysUserMapper sysUserMapper) {
        this.sysUserMapper = sysUserMapper;
    }

    @Nullable
    @Override
    public Mono<SysUser> getById(Long id) {
        return sysUserMapper.findById(id);
    }

    @Override
    public Flux<SysUser> page(Integer page, Integer size) {
        return sysUserMapper.findAll();
    }

}
