package com.conwayd.baseserver.service;


import com.conwayd.baseserver.entity.SysUser;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SysUserService {

    Mono<SysUser> getById(Long id);

    Flux<SysUser> page(Integer page, Integer size);
}
