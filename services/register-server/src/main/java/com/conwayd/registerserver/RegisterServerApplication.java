/*
 * loongx-cloud
 *
 * Copyright 2024 conwayd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.conwayd.registerserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * RegisterServerApplication
 *
 * @author conwayD at 2024-02-29 16:03
 * @since 1.0.0
 */
@EnableEurekaServer
@SpringBootApplication(excludeName = {
        "org.springframework.cloud.client.loadbalancer.LoadBalancerAutoConfiguration",
        "org.springframework.cloud.client.loadbalancer.reactive.LoadBalancerBeanPostProcessorAutoConfiguration"
})
public class RegisterServerApplication {

    private static final Logger log = LoggerFactory.getLogger(RegisterServerApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(RegisterServerApplication.class, args);
        log.info(">>>> local port:{}", context.getEnvironment().getProperty("server.port"));
    }

}
