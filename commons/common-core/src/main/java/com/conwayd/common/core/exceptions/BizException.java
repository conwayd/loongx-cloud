/*
 * loongx-cloud
 *
 * Copyright 2024 conwayd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.conwayd.common.core.exceptions;

import com.conwayd.common.core.constants.ApiCodeConstant;

import java.io.Serial;

/**
 * 业务异常
 *
 * @author conwayD at 2024-02-27 15:48
 * @since 1.0.0
 */
public class BizException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = -5927812723235954746L;

    /**
     * 异常码
     */
    private final Integer errorCode;

    /**
     * 人性化的异常信息，返回给客户端
     */
    private String humanizedMessage;

    public BizException() {
        super("操作失败");
        this.errorCode = ApiCodeConstant.OPERATION_FAILED;
    }

    public BizException(Throwable e) {
        super("操作失败", e);
        this.errorCode = ApiCodeConstant.OPERATION_FAILED;
    }

    public BizException(String humanizedMessage) {
        super("操作失败");
        this.errorCode = ApiCodeConstant.OPERATION_FAILED;
        this.humanizedMessage = humanizedMessage;
    }

    public BizException(String humanizedMessage, Throwable e) {
        super("操作失败", e);
        this.errorCode = ApiCodeConstant.OPERATION_FAILED;
        this.humanizedMessage = humanizedMessage;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getHumanizedMessage() {
        return humanizedMessage;
    }

    public void setHumanizedMessage(String humanizedMessage) {
        this.humanizedMessage = humanizedMessage;
    }
}
