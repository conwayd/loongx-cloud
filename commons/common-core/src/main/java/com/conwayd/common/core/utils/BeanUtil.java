/*
 * loongx-cloud
 *
 * Copyright 2024 conwayd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.conwayd.common.core.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 * bean工具类
 *
 * @author conwayD at 2024-03-04 14:40
 * @since 1.0.0
 */
@Component
public class BeanUtil implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    @Nullable
    public static <T> T getBean(Class<T> tClass) {
        if (tClass == null) {
            return null;
        }
        return context.getBean(tClass);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String className) {
        if (className == null || className.isEmpty()) {
            return null;
        }
        return (T) context.getBean(className);
    }

    @Nullable
    public static <T> T getBean(String className, Class<T> tClass) {
        if (tClass == null) {
            return null;
        }
        return context.getBean(className, tClass);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String className, Object... args) {
        if (className == null || className.isEmpty()) {
            return null;
        }
        return (T) context.getBean(className, args);
    }

    @Nullable
    public static <T> T getBean(Class<T> tClass, Object... args) {
        if (tClass == null) {
            return null;
        }
        return context.getBean(tClass, args);
    }

}
