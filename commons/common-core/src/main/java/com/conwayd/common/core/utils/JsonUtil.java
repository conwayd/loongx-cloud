/*
 * loongx-cloud
 *
 * Copyright 2024 conwayd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.conwayd.common.core.utils;

import com.conwayd.common.core.constants.CommonConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.boot.autoconfigure.jackson.JacksonProperties;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

/**
 * json工具类
 *
 * @author conwayD at 2024-03-04 16:17
 * @since 1.0.0
 */
public class JsonUtil {

    /**
     * 默认序列化器
     */
    private static ObjectMapper OBJECT_MAPPER_DEFAULT = null;
    /**
     * 忽略null
     */
    private static ObjectMapper OBJECT_MAPPER_IGNORE_NULL = null;
    /**
     * 忽略未知属性
     */
    private static ObjectMapper OBJECT_MAPPER_IGNORE_UNKNOWN = null;

    static {
        init(null);
    }

    /**
     * 根据环境上下文jackson配置刷新 ObjectMapper
     *
     * @param jacksonProperties 上下文jackson配置
     */
    public static void refreshInit(@Nullable JacksonProperties jacksonProperties) {
        init(jacksonProperties);
    }

    @NonNull
    public static ObjectMapper getDefault() {
        return OBJECT_MAPPER_DEFAULT;
    }

    @NonNull
    public static ObjectMapper getIgnoreNull() {
        return OBJECT_MAPPER_IGNORE_NULL;
    }

    @NonNull
    public static ObjectMapper getIgnoreUnknown() {
        return OBJECT_MAPPER_IGNORE_UNKNOWN;
    }

    /**
     * object -> json string
     * 序列化结果包含null
     */
    @Nullable
    public static String toJson(Object object) {
        try {
            if (object == null) {
                return null;
            }
            return OBJECT_MAPPER_DEFAULT.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * object -> json string
     *
     * @param nullable null 或 true：序列化结果包含null；否则不包含
     */
    @Nullable
    public static String toJson(Object object, Boolean nullable) {
        try {
            if (object == null) {
                return null;
            }
            if (Boolean.FALSE.equals(nullable)) {
                return OBJECT_MAPPER_IGNORE_NULL.writeValueAsString(object);
            }
            return toJson(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * json string -> object
     *
     * @param Strict true：严格模式，存在未知属性报错
     */
    @Nullable
    public static <T> T fromJson(String json, Class<T> tClass, Boolean Strict) {
        try {
            if (json == null || json.isEmpty()) {
                return null;
            }
            if (Boolean.TRUE.equals(Strict)) {
                return OBJECT_MAPPER_DEFAULT.readValue(json, tClass);
            }
            return OBJECT_MAPPER_IGNORE_UNKNOWN.readValue(json, tClass);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * json string -> object
     *
     * @param Strict true：严格模式，存在未知属性报错
     */
    @Nullable
    public static <T> T fromJson(String json, TypeReference<T> typeReference, Boolean Strict) {
        try {
            if (json == null || json.isEmpty()) {
                return null;
            }
            if (Boolean.TRUE.equals(Strict)) {
                return OBJECT_MAPPER_DEFAULT.readValue(json, typeReference);
            }
            return OBJECT_MAPPER_IGNORE_UNKNOWN.readValue(json, typeReference);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * json string -> object
     *
     * @param Strict true：严格模式，存在未知属性报错
     */
    @Nullable
    public static <T> T fromJson(String json, JavaType type, Boolean Strict) {
        try {
            if (json == null || json.isEmpty()) {
                return null;
            }
            if (Boolean.TRUE.equals(Strict)) {
                return OBJECT_MAPPER_DEFAULT.readValue(json, type);
            }
            return OBJECT_MAPPER_IGNORE_UNKNOWN.readValue(json, type);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 默认初始化
     * <pre>
     * 当 jackson配置 为 null 时，使用默认配置
     * 时间格式默认使用 {@link CommonConstant#DATETIME_PATTERN}
     * 时区默认东八区
     * </pre>
     *
     * @param jacksonProperties jackson配置
     */
    private static void init(@Nullable JacksonProperties jacksonProperties) {
        if (OBJECT_MAPPER_DEFAULT == null) {
            OBJECT_MAPPER_DEFAULT = new ObjectMapper();
        }

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(CommonConstant.DATETIME_PATTERN.substring(0, 10));
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(CommonConstant.DATETIME_PATTERN.substring(11));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(CommonConstant.DATETIME_PATTERN);
        TimeZone timeZone = CommonConstant.TIME_ZONE_GMT_E8;

        if (jacksonProperties != null) {
            String dateFormat = jacksonProperties.getDateFormat();
            if (dateFormat != null && !dateFormat.isEmpty()) {
                dateFormatter = DateTimeFormatter.ofPattern(dateFormat.substring(0, 10));
                timeFormatter = DateTimeFormatter.ofPattern(dateFormat.substring(11));
                dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
            }
            TimeZone zone = jacksonProperties.getTimeZone();
            if (zone != null) {
                timeZone = zone;
            }
        }

        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(dateFormatter));
        javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer(timeFormatter));
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(dateTimeFormatter));

        javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer(dateFormatter));
        javaTimeModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer(timeFormatter));
        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(dateTimeFormatter));

        OBJECT_MAPPER_DEFAULT.registerModule(javaTimeModule);

        /*
         * 根据需要开启 long -> string
         *
         * SimpleModule simpleModule = new SimpleModule();
         * simpleModule.addSerializer(BigInteger.class, ToStringSerializer.instance);
         * simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
         * simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
         *
         * OBJECT_MAPPER_DEFAULT.registerModules(javaTimeModule, simpleModule);
         */

        OBJECT_MAPPER_DEFAULT.setTimeZone(timeZone);
        OBJECT_MAPPER_DEFAULT.setDateFormat(new SimpleDateFormat(CommonConstant.DATETIME_PATTERN));

        OBJECT_MAPPER_IGNORE_NULL = OBJECT_MAPPER_DEFAULT.copy();
        OBJECT_MAPPER_IGNORE_NULL.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        OBJECT_MAPPER_IGNORE_UNKNOWN = OBJECT_MAPPER_DEFAULT.copy();
        OBJECT_MAPPER_IGNORE_UNKNOWN.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }
}
