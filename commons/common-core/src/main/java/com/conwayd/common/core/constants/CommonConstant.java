/*
 * loongx-cloud
 *
 * Copyright 2024 conwayd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.conwayd.common.core.constants;

import java.time.ZoneId;
import java.util.TimeZone;

/**
 * 全局常量
 *
 * @author conwayD at 2024-02-27 15:46
 * @since 1.0.0
 */
public interface CommonConstant {

    String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    String ZONE_GMT = "GMT";
    String ZONE_E8 = "+8";
    String ZONE_GMT_E8 = ZONE_GMT + ZONE_E8;
    TimeZone TIME_ZONE_GMT_E8 = TimeZone.getTimeZone(ZoneId.of(CommonConstant.ZONE_GMT_E8));

}
