/*
 * loongx-cloud
 *
 * Copyright 2024 conwayd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.conwayd.common.core.utils;

/**
 * 断言工具类
 *
 * @author conwayD at 2024-02-29 16:33
 * @since 1.0.0
 */
public class AssertUtil {

    /**
     * 条件与执行
     *
     * @param condition       条件成立时则执行
     * @param otherwiseAction 条件不成立时则执行
     */
    public static void assertAndRun(boolean condition, Runnable trueAction, Runnable otherwiseAction) {
        if (trueAction == null || otherwiseAction == null) {
            return;
        }

        if (condition) {
            trueAction.run();
        } else {
            otherwiseAction.run();
        }
    }

}
