/*
 * loongx-cloud
 *
 * Copyright 2024 conwayd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.conwayd.common.datasource.utils;

import org.springframework.jdbc.support.JdbcUtils;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.Map;

/**
 * jdbc结果映射工具
 *
 * @author conwayD at 2024-03-04 15:30
 * @since 1.0.0
 */
public class ResultSetMapUtil {

    public static <T> void mapModel(ResultSet resultSet, Map<String, Field> fieldMap, T targetModel) {
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 0; i < metaData.getColumnCount(); i++) {
                Object rsv = JdbcUtils.getResultSetValue(resultSet, i + 1);
                String columnName = metaData.getColumnLabel(i + 1).replace("_", "").toLowerCase();
                Field field = fieldMap.get(columnName);
                if (field != null) {
                    try {
                        field.set(targetModel, rsv);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
