package com.conwayd.common.web.configs;

import com.conwayd.common.core.utils.JsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.lang.NonNull;

@Configuration
@AutoConfigureBefore(value = {JacksonAutoConfiguration.class})
@EnableConfigurationProperties(value = {JacksonProperties.class})
public class JacksonConfiguration {

    @Bean
    public Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder(@NonNull JacksonProperties jacksonProperties) {
        JsonUtil.refreshInit(jacksonProperties);
        ObjectMapper objectMapper = JsonUtil.getIgnoreUnknown();

        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.configure(objectMapper);

        return builder;
    }

}
