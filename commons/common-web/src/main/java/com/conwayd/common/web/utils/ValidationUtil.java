package com.conwayd.common.web.utils;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ValidationUtil {

    private static Validator validator;

    @Autowired
    public void setValidator(Validator bean) {
        validator = bean;
    }

    /**
     * 手动参数校验
     *
     * @param object 被校验对象
     * @param groups 校验所属组
     * @throws ConstraintViolationException 校验不通过异常
     */
    public static void validate(Object object, Class<?>... groups) throws ConstraintViolationException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
    }
}
